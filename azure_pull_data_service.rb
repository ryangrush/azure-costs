require 'dotenv/load'
require 'pry'
require 'json'
require 'azure_mgmt_consumption'
require 'azure_mgmt_monitor'
require 'uri'
require 'rest-client'
require 'chronic'
require 'csv'
require 'active_support/time'

class AzurePullDataService

  attr_reader :rate_card, :provider, :header
  attr_accessor :cached_owners

  def initialize
    Dotenv.overload('.env')

    Time.zone = 'UTC'
    Chronic.time_class = Time.zone

    @provider = MsRestAzure::ApplicationTokenProvider.new(
      ENV['AZURE_TENANT_ID'],
      ENV['AZURE_CLIENT_ID'],
      ENV['AZURE_CLIENT_SECRET']
    )

    @header = provider.get_authentication_header
    
    get_cached_owners
    get_rate_card
  end

  def get_current_records
    start_time = Chronic.parse("1st DATE of this month").strftime("%F")
    end_time   = DateTime.now.strftime("%F")
    period     = DateTime.now.strftime("%Y%m")
    usage_url  = URI("https://management.azure.com/subscriptions/#{ENV['AZURE_SUBSCRIPTION_ID']}/providers/Microsoft.Consumption/usageDetails?$filter=properties/usageEnd ge #{start_time} AND properties/usageEnd le #{end_time}&api-version=2019-01-01").to_s
    results    = get_paginated_results(usage_url).sort_by{|x| x["properties"]["usageStart"]}

    log_results(results, period)
  end

  def get_historical_records
    start_date = Chronic.parse("Jan 1, 2019")
    end_date = Chronic.parse("1st DATE of this month") - 1.day

    (start_date.year..end_date.year).each do |year|
      is_starting_year_and_is_not_end_year = year < end_date.year && year == start_date.year
      is_starting_year_and_is_end_year = year == end_date.year && year == start_date.year
      is_end_year = year == end_date.year
      start_month = is_starting_year_and_is_not_end_year ? start_date.month : 01
      start_month = is_starting_year_and_is_end_year ? start_date.month : start_month
      end_month = is_end_year ? end_date.month : 12

      (start_month..end_month).each do |month|
        month_as_string = month.to_s.rjust(2, "0")
        period = "#{year}#{month_as_string}"
        usage_url  = URI("https://management.azure.com/subscriptions/#{ENV['AZURE_SUBSCRIPTION_ID']}/providers/Microsoft.Billing/billingPeriods/#{period}/providers/Microsoft.Consumption/usageDetails?&api-version=2019-01-01").to_s
        results    = get_paginated_results(usage_url)

        log_results(results, period) if results
      end
    end
  end

  private

    def log_results results, period=nil
      puts "#{period} --- #{results.try(:count)}" if results.class != String
      rows = []

      results.each_with_index do |instance, i|
        resource_id      = instance["properties"]["instanceId"]
        owner            = get_owner_of_resource(resource_id)
        rate             = rate_card[instance["properties"]["meterId"]]
        pretax_cost      = instance["properties"]["pretaxCost"].round(2)
        consumed_service = instance["properties"]["consumedService"]
        usage_start      = Chronic.parse(instance["properties"]["usageStart"]).strftime("%F")
        usage_end        = Chronic.parse(instance["properties"]["usageEnd"]).strftime("%F")
        cost_per_day     = get_cost_per_day(instance, rate)

        @cached_owners[resource_id] = owner

        rows << [pretax_cost, consumed_service, usage_start, usage_end, owner, cost_per_day, resource_id]

        puts "#{i} / #{results.count} ... #{usage_start}"
        if i % 100 == 0 || (results.count - 1 == i)
          rows = rows.sort{|a,b| b[0] <=> a[0]}
          write_instance_results_to_csv(rows, period)

          File.open('data/owners.json', 'w') { |file| file.write(JSON.pretty_generate(@cached_owners)) }
        end
      end
    end

    def get_rate_card
      @rate_card = {}
      offer = "MS-AZR-0044P"
      url = URI("https://management.azure.com/subscriptions/#{ENV['AZURE_SUBSCRIPTION_ID']}/providers/Microsoft.Commerce/RateCard?api-version=2015-06-01-preview&%24filter=OfferDurableId+eq+%27#{offer}%27+and+Locale+eq+%27en-US%27+and+Regioninfo+eq+%27US%27+and+Currency+eq+%27USD%27")
      results = RestClient.get(url.to_s, headers={Authorization: provider.get_authentication_header})

      JSON.parse(results)["Meters"].each do |x|
        @rate_card[x["MeterId"]] = x
      end

      @rate_card
    end

    def get_cost_per_day instance, rate
      if rate && rate["Unit"].include?("Hour")
        (rate["MeterRates"]["0"] * 24).round(2)
      elsif rate && rate["Unit"].match('1\/Month')
        (rate["MeterRates"]["0"] / (365 / 12)).round(2)
      else
        nil
      end
    end

    def get_paginated_results url, results=[]
      begin
        response = RestClient.get(url.to_s, headers={Authorization: header})

        if response.code == 200
          page_results = JSON.parse(response)
          url = page_results["nextLink"]
          values = page_results["value"]
          results = results + values

          if url
            get_paginated_results(url, results)
          else
            results
          end
        end
      rescue Exception => e
        p e.to_s # Rescues `401 Unauthorized` errors
      end
    end

    def get_owner_of_resource resource_id
      start_time = Chronic.parse("3 months ago").strftime("%F")
      end_time   = DateTime.now.strftime("%F")
      owner      = @cached_owners[resource_id]

      if !@cached_owners.key?(resource_id)
        activity_logs_url = URI("https://management.azure.com/subscriptions/#{ENV['AZURE_SUBSCRIPTION_ID']}/providers/microsoft.insights/eventtypes/management/values?api-version=2015-04-01&$filter=resourceId eq #{resource_id} and eventTimestamp ge #{start_time} and eventTimestamp le #{end_time}").to_s
        activity_logs_results = get_paginated_results(activity_logs_url)
        owner = activity_logs_results && activity_logs_results.class != String ? activity_logs_results.reverse.collect{|x| x["caller"]}.detect{|x| x ? x.include?("@") : nil} : nil
      end

      owner
    end

    def write_instance_results_to_csv rows, period
      CSV.open("data/#{period}.csv", "w") do |csv|
        csv << ["Cost", "Service", "Start", "End", "Owner", "Cost Per Day", "Resource Id"]
        rows.each do |row|
          csv << row
        end
      end
    end

    def get_cached_owners
      @cached_owners = JSON.parse(File.read('data/owners.json'))
    end

end


# AzurePullDataService.new.get_historical_records
AzurePullDataService.new.get_current_records


