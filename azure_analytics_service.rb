require 'dotenv/load'
require 'pry'
require 'json'
require 'terminal-table'
require 'chronic'
require 'csv'
require 'active_support/time'

class AzureAnalyticsService

  def initialize
    Time.zone = 'UTC'
    Chronic.time_class = Time.zone
  end

  def last_30_days_usage
    current_month   = DateTime.now.strftime("%m").to_i
    current_year    = DateTime.now.strftime("%Y").to_i
    last_month      = current_month - 1 == 0 ? 12 : current_month - 1
    last_year       = current_month == 12 ? current_year - 1 : current_year
    current_period  = DateTime.now.strftime("%Y%m")
    last_period     = "#{last_year}#{format('%02d', last_month)}"
    start_date      = Chronic.parse("7 days ago")
    last_two_months = {}


    [current_period, last_period].each do |file_name|
      CSV.foreach("data/#{file_name}.csv", :headers => true) do |row|
        cost_incurred_in_the_last_30_days = Chronic.parse(row["End"]) > start_date

        if cost_incurred_in_the_last_30_days
          last_two_months[row["Owner"]] ||= 0
          last_two_months[row["Owner"]] += row["Cost"].to_f
          last_two_months[row["Owner"]] = last_two_months[row["Owner"]].round(2)
        end
      end
    end

    puts Terminal::Table.new(rows: last_two_months.sort_by{|key, value| value}.reverse)
  end

end


AzureAnalyticsService.new.last_30_days_usage

